# oereb-iconizer

## TODO
- Tests anpassen an neue Layernamen. Wird sowieso failen.
- ~~app name?~~
- ~~version?~~
- release

## Funktionsweise
_Oereb-iconizer_ hat folgende Funktionen:

1. Erstellung von Einzelsymbolen (`BufferedImage`) anhand einer "GetStyles"- und "GetLegendGraphic"-Antwort.
2. Speicherung der Einzelsymbole auf dem Filesystem.
3. Erstellung einer INTERLIS-Transferdatei mit den Symbolen als `BLACKBOX BINARY`.

Implementiert ist die Erstellung der Symbole (Schritt 1) für QGIS-Server 3.x. Andere Implementierungen müssen sich vor allem um den unterschiedlichen Aufruf des Einzelsymbole-GetLegenendGraphic-Requests und die Parse-Logik der GetStyles-Antwort kümmern. 

Damit der gesamte Prozess relativ simpel und durchschaubar bleibt, gibt es verschiedene harte und einschränkende Rahmenbedingungen: Die Definition der Style in QGIS muss so gemacht werden, dass es pro Artcode (in einem Thema) eine Rule gibt. Entscheidend ist jedoch das resultierende SLD (aus dem GetStyles-Request):

```
<se:Rule>
    <se:Name>Wohnzone 1 G</se:Name>
    <se:Description>
        <se:Title>Wohnzone 1 G</se:Title>
    </se:Description>
    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
        <ogc:PropertyIsEqualTo>
            <ogc:Function name="substr">
                <ogc:PropertyName>artcode</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
                <ogc:Literal>3</ogc:Literal>
            </ogc:Function>
            <ogc:Literal>110</ogc:Literal>
        </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <se:PolygonSymbolizer>
        <se:Fill>
            <se:SvgParameter name="fill">#ffff33</se:SvgParameter>
        </se:Fill>
    </se:PolygonSymbolizer>
</se:Rule>
```

Eine `Rule` muss ein `Name`- und ein `Filter`-Element haben. Der `Filter` muss ein `PropertyIsEqualTo`-Element haben. `PropertyIsEqualTo` muss ein `Literal`-Element haben, dessen Wert dem ÖREB-Artcode entspricht. Die Funktion kann beliebig sein. Anhand des Namens wird anschliessend ein spezielle "GetLegendGraphic"-Request gemacht und die Antwort (das einzelne Symbol) zusammen mit dem Artcode gespeichert.

Werden die Symbole auf dem Filesystem gespeichert, erhalten die Symbole als Namen den Wert des Artcodes. B

Bei der Erstellung der INTERLIS-Transferdatei müssen zwingend das Thema und die Artcodeliste mitgeliefert werden, damit ein spätere Zuweisung in der Datenbank mit SQL sichergestellt ist. Je nach Arbeitsweise und Thema wären wahrscheinlich beide Informationen nicht wirklich zwingend. 

## Anleitung (SO!GIS)

Falls Style verändert werden müssen oder neue dazu kommen, muss zuerst das QGIS-Projekt "oereb-symbols.qgs" nachgeführt werden. Dies wird im Git-Repository "oereb-wms" gemacht. Die Pipeline erstellt nach dem Pushen ein neues `sogis/oereb-wms` Docker Image. Anschliessend kann der Docker Container gestartet werden:

```
docker run -p 8083:80 sogis/oereb-wms:2.0
```

_Oereb-iconizer_ kann als Fatjar hier https://gitlab.com/sogis/oereb-v2/oereb-iconizer/-/releases heruntergeladen werden (siehe Link "Fat jar"). Der Link zeigt auf einen Artifact der Buildpipeline. Falls dieser nach einer gewissen Zeit automatisch gelöscht wird, muss in Betracht gezogen werden, die Fatjar-Datei im Maven-Repo zu publizieren.

Erstellen der Symbole und der INTERLIS-Transferdatei:
```
java -jar oereb-iconizer-2.0.XX-all.jar --sldUrl="http://localhost:8083/wms/oereb-symbols?SERVICE=WMS&REQUEST=GetStyles&LAYERS=ch.StatischeWaldgrenzen&SLD_VERSION=1.1.0" --legendGraphicUrl="http://localhost:8083/wms/oereb-symbols?SERVICE=WMS&REQUEST=GetLegendGraphic&LAYER=ch.StatischeWaldgrenzen&FORMAT=image/png&RULELABEL=false&LAYERTITLE=false&HEIGHT=35&WIDTH=70&SYMBOLHEIGHT=3&SYMBOLWIDTH=6&DPI=300" --fileName=fubar.xtf --basketId=ch.so.agi.oereb.legendeneintraege --theme=ch.StatischeWaldgrenzen --typeCodeList=urn:fdc:ilismeta.interlis.ch:2017:Typ_Kanton_Waldgrenzen
```

## Testrequests

Testrequest GetCapabilites:
```
http://localhost:8083/wms/oereb-symbols?SERVICE=WMS&REQUEST=GetCapabilities
```

Testrequest GetStyles (Statische Waldgrenzen):
```
http://localhost:8083/wms/oereb-symbols?&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetStyles&LAYERS=ch.StatischeWaldgrenzen&STYLE=default&SLD_VERSION=1.1.0
```

Testrequest GetLegendGraphic (Statische Waldgrenzen):
```
http://localhost:8083/wms/oereb-symbols?&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetLegendGraphic&LAYER=ch.StatischeWaldgrenzen&FORMAT=image/png&STYLE=default&SLD_VERSION=1.1.0
```

Testrequest GetLegendGrapic (Einzelnes Symbol):
```
http://localhost:8083/wms/oereb-symbols?SERVICE=WMS&REQUEST=GetLegendGraphic&LAYER=ch.StatischeWaldgrenzen&FORMAT=image/png&RULELABEL=false&LAYERTITLE=false&HEIGHT=35&WIDTH=70&SYMBOLHEIGHT=3&SYMBOLWIDTH=6&DPI=300&RULE=in+Bauzonen
```

